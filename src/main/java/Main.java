import java.time.LocalDateTime;

public class  Main {

    public static void main(String[] args) {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.now(),9.0);
        var movie_ticket = new MovieTicket(movie_screening,1,0,false);
        var order = new Order(1,false);
        order.addSeatReservation(movie_ticket);
        System.out.println("Order price for 1 ticket as non-student or premium: "+order.calculatePrice());
        order.addSeatReservation(movie_ticket);
        System.out.println("Order price for 2 tickets as non-student or premium: "+order.calculatePrice());
    }
}
