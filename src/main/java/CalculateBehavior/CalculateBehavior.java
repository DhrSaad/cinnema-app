import java.util.ArrayList;

public interface CalculateBehavior {
     double calculatePrice(ArrayList<MovieTicket> tickets);
}
