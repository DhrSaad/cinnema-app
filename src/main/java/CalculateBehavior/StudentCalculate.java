import java.util.ArrayList;

public class StudentCalculate implements CalculateBehavior {
    @Override
    public double calculatePrice(ArrayList<MovieTicket> tickets) {
        var price = 0.0;
        for (int i = 0; i < tickets.size(); i++) {
            var ticket = tickets.get(i);
            var ticket_price = ticket.getPrice();
            // eerst premium erbij tellen, dan pas korting
            if (tickets.get(i).isPremiumTicket()) {
                ticket_price += 2.0;
            }
            var day_of_week = ticket.dayOfWeek().getValue();
            // student die in t weekend ook 6 kaartjes koopt = 10%
            if (day_of_week > 5) {
                ticket_price *= 0.9;
            }
            if (i != 1) {
                price += ticket_price;
            } else {
                System.out.println("Ticket gratis!");
            }
        }
        price *= 0.9;

        return price;
    }
}
