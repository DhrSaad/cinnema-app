public class SMSAdapter extends NotificationService {

    public SMSAdapter(OrderState orderState) {
        super(orderState);
    }

    @Override
    String getService() {
        return "SMS";
    }
}
