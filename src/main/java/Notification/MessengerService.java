public class MessengerService implements Observer {
    NotificationService service;
    OrderState orderState;

    public MessengerService(NotificationService service) {
        this.service = service;
        this.orderState = service.orderState;
    }

    @Override
    public void update(OrderState orderState) {
        // when it's still in create state, you're allowed to edit it. messenger should notify when it's edited.
        if (!this.orderState.getClass().getName().equals(orderState.getClass().getName()) ||
                orderState.getClass().getName().equals(CreatedState.class.getName())) {
            System.out.println("Order state changed! " + orderState.getClass().getName());
            service.setOrderState(orderState);
            service.sendNotification();
        }
        this.orderState = orderState;
    }

}
