public class WhatsappAdapter extends NotificationService {
    public WhatsappAdapter(OrderState orderState) {
        super(orderState);
    }

    @Override
    String getService() {
        return "WhatsApp";
    }
}
