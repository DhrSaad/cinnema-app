public interface Observer {
    void update(OrderState orderState);
}
