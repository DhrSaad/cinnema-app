public class EmailAdapter extends NotificationService {

    public EmailAdapter(OrderState orderState) {
        super(orderState);
    }

    @Override
    String getService() {
        return "Email";
    }

}
