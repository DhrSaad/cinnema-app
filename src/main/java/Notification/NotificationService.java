public abstract class NotificationService {
    OrderState orderState;

    public NotificationService(OrderState orderState) {
        this.orderState = orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public OrderState getOrderState() {
        return this.orderState;
    }

    final void sendNotification() {
        var notification = setupNotification();
        System.out.println(notification+" sent!");
    }

    String setupNotification() {
        var service = getService();
        getAddressInfo();
        installAddressInfo();
        setMessage();
        if (isPaidState()) {
            createAttachment();
        }
        return service;
    }

    private void createAttachment() {

    }

    boolean isPaidState() {
        //TODO maybe improve? loosely coupled with name rn
        return orderState.getClass().getName().equals(PaidState.class.getName());
    }

    abstract String getService();

    void getAddressInfo() {
    }

    void installAddressInfo() {
    }

    void setMessage() {
    }
}
