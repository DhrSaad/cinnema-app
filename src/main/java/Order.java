import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Order implements Observable {
    int OrderNr;
    public boolean paid;
    public CalculateBehavior calculateBehavior;
    ExportBehavior exportBehavior;
    public ArrayList<MovieTicket> tickets = new ArrayList<>();
    public LocalDateTime lastUpdated;

    OrderState createdState;
    OrderState cancelledState;
    OrderState paidState;
    OrderState reservedState;
    OrderState provisionalState;
    OrderState deliveredState;

    OrderState orderState;

    final List<Observer> observers = new ArrayList<>();

    public Order(int orderNr, boolean isStudentOrder) {
        OrderNr = orderNr;
        if (isStudentOrder) {
            calculateBehavior = new StudentCalculate();
        } else {
            calculateBehavior = new NormalCalculate();
        }
        this.lastUpdated = LocalDateTime.now();

        this.createdState = new CreatedState(this);
        this.cancelledState = new CancelledState(this);
        this.paidState = new PaidState(this);
        this.reservedState = new ReservedState(this);
        this.provisionalState = new ProvisionalState(this);
        this.deliveredState = new DeliveredState(this);

        this.orderState = createdState;
    }

    public double calculatePrice() {
        return orderState.calculatePrice();
    }

    public void addSeatReservation(MovieTicket ticket) {
        orderState.addSeatReservation(ticket);
    }

    public void setExportBehavior(ExportBehavior exportBehavior) {
        this.exportBehavior = exportBehavior;
    }

    public int getOrderNr() {
        return OrderNr;
    }

    public void export() {
        //TODO
    }

    public void submit() {
        orderState.submit();
    }

    public void pay() {
        orderState.pay();
    }

    public void sendTickets() {
        orderState.sendTickets();
    }

    public void cancel() {
        orderState.cancel();
    }

    void    setOrderState(OrderState orderState) {
        if (
                orderState == provisionalState ||
                        orderState == createdState ||
                        orderState == cancelledState ||
                        orderState == paidState
        ) {

            System.out.println("Notifying observers!");
            this.orderState = orderState;
            notifyObservers();
        } else {
            this.orderState = orderState;
        }

    }


    public String getState() {
        return this.orderState.getClass().getName();
    }


    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (var x : observers) {
            x.update(orderState);
        }
    }
}
