import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ReservedState implements OrderState {
    Order order;

    public ReservedState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        return order.calculateBehavior.calculatePrice(order.tickets);
    }

    @Override
    public void addSeatReservation(MovieTicket ticket) {
        order.tickets.add(ticket);
    }

    @Override
    public void pay() {
        order.paid = true;
        System.out.println("Payment successful!");
    }

    @Override
    public void sendReminder() {

    }

    public void submit() {
        if (order.paid) {
            order.setOrderState(new PaidState(order));
        } else if (ChronoUnit.HOURS.between(order.lastUpdated,LocalDateTime.now()) >= 24){
            order.lastUpdated = LocalDateTime.now();
            order.setOrderState(new ProvisionalState(order));
        }
    }

    @Override
    public void sendTickets() {

    }

    @Override
    public void cancel() {
        order.setOrderState(new CancelledState(order));
    }

}
