
public interface OrderState {

     double calculatePrice();


    void addSeatReservation(MovieTicket ticket);

    void submit();

    void pay();

    void sendReminder();

    void cancel();

    void sendTickets();
}
