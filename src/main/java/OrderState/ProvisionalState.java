
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ProvisionalState implements OrderState {
    Order order;

    public ProvisionalState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        return order.calculateBehavior.calculatePrice(order.tickets);
    }

    @Override
    public void addSeatReservation(MovieTicket ticket) {
        // not allowed!
    }

    @Override
    public void pay() {
        order.paid = true;
        System.out.println("Payment successful!");
    }

    @Override
    public void sendReminder() {

    }

    @Override
    public void cancel() {
order.setOrderState(new CancelledState(order));
    }

    @Override
    public void submit() {
        if (order.paid) {
            order.setOrderState(new PaidState(order));
        } else {
            if (ChronoUnit.HOURS.between(order.lastUpdated,LocalDateTime.now()) >= 12) {
                order.lastUpdated = LocalDateTime.now();
                System.out.println("No payment has been made, order cancelled");
                order.setOrderState(new CancelledState(order));
            }
        }
    }

    @Override
    public void sendTickets() {

    }
}
