
public class PaidState implements OrderState{
    Order order;

    public PaidState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        return order.calculateBehavior.calculatePrice(order.tickets);
    }

    @Override
    public void addSeatReservation(MovieTicket ticket) {
        // not allowed!
    }

    @Override
    public void submit() {
//
    }

    @Override
    public void pay() {
//
    }

    @Override
    public void sendReminder() {

    }

    @Override
    public void cancel() {

    }


    @Override
    public void sendTickets() {
        order.setOrderState(new DeliveredState(order));
    }
}
