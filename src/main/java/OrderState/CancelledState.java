
public class CancelledState implements OrderState {
    final Order order;

    public CancelledState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        return order.calculateBehavior.calculatePrice(order.tickets);
    }


    @Override
    public void addSeatReservation(MovieTicket ticket) {
        // not allowed!
    }

    @Override
    public void pay() {

    }

    @Override
    public void sendReminder() {

    }

    @Override
    public void cancel() {

    }

    @Override
    public void submit() {

    }

    @Override
    public void sendTickets() {

    }
}
