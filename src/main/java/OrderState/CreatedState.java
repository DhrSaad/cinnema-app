
public class CreatedState implements OrderState{
    final Order order;

    public CreatedState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        return order.calculateBehavior.calculatePrice(order.tickets);
    }

    @Override
    public void addSeatReservation(MovieTicket ticket) {
        order.tickets.add(ticket);
    }

    @Override
    public void submit() {
        order.setOrderState(new ReservedState(order));
    }

    @Override
    public void pay() {
    }

    @Override
    public void sendReminder() {

    }

    @Override
    public void cancel() {

    }

    @Override
    public void sendTickets() {

    }

}
