import java.util.ArrayList;

public class Movie {
    String title;
    ArrayList<MovieScreening> screenings = new ArrayList<>();

    @Override
    public String toString() {
        return title;
    }

    public void addScreening(MovieScreening screening) {
        screenings.add(screening);
    }

    public Movie(String title) {
        this.title = title;
    }
}
