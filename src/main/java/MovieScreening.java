import java.time.LocalDateTime;

public class MovieScreening {
    LocalDateTime dateAndTime;
    double pricePerSeat;
    Movie movie;

    public MovieScreening(Movie movie, LocalDateTime dateAndTime, double pricePerSeat) {
        this.dateAndTime = dateAndTime;
        this.pricePerSeat = pricePerSeat;
        this.movie = movie;
    }


    public double getPricePerSeat() {
        return pricePerSeat;
    }

    @Override
    public String toString() {
        return "Movie screening on " + dateAndTime.toString() +
                ", price: " + pricePerSeat + ",-";
    }
}
