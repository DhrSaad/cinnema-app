import java.time.DayOfWeek;

public class MovieTicket {
    int rowNr;
    int seatNr;
    boolean isPremium;
    MovieScreening movieScreening;

    public MovieTicket(MovieScreening movieScreening, int rowNr, int seatNr, boolean isPremium) {
        this.rowNr = rowNr;
        this.seatNr = seatNr;
        this.isPremium = isPremium;
        this.movieScreening = movieScreening;
    }

    public DayOfWeek dayOfWeek() {
        var day_of_week =  movieScreening.dateAndTime.getDayOfWeek();
        System.out.println("Day of week: " + day_of_week +" value: "+day_of_week.getValue());
        return day_of_week;
    }

    public boolean isPremiumTicket() {
        return isPremium;
    }

    public double getPrice() {
        return movieScreening.getPricePerSeat();
    }

    public String toString() {
        return "Row: " + rowNr +
                ", Seat: " + seatNr +
                ", Premium ticket: " + ((isPremium) ? "yes" : "no");
    }
}
