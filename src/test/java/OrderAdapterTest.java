import org.junit.Assert;
import org.junit.Test;

public class OrderAdapterTest {
    @Test
    public void adapter_should_change_return_based_on_type() {
        var order = new Order(0, false);
        var email_adapter = new EmailAdapter(order.orderState);

        Assert.assertEquals(email_adapter.getService(), "Email");
    }

    @Test
    public void adapter_should_update_when_state_changes() {
        var order = new Order(0, false);
        var email_adapter = new EmailAdapter(order.orderState);
        var observer = new MessengerService(email_adapter);
        order.registerObserver(observer);

        Assert.assertFalse(email_adapter.isPaidState());
        order.setOrderState(order.paidState);
        Assert.assertTrue(email_adapter.isPaidState());
    }
}
