import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class OrderStateUnitTest {
    @Test
    public void order_should_create_with_created_state() {
        var order = new Order(1,false);
        Assert.assertEquals(order.getState(), CreatedState.class.getName());
    }

    @Test
    public void order_should_go_to_reserved_when_submitted() {
        var order = new Order(1,false);
        order.submit();
        Assert.assertEquals(order.getState(), ReservedState.class.getName());
    }
    @Test
    public void order_should_go_to_paid_when_submitted_and_paid() {
        var order = new Order(1,false);
        order.submit();
        order.pay();
        order.submit();
        Assert.assertEquals(order.getState(), PaidState.class.getName());
    }
    @Test
    public void order_should_go_to_delivered_when_submitted_and_paid_and_tickets_are_sent() {
        var order = new Order(1,false);
        order.submit();
        order.pay();
        order.submit();
        order.sendTickets();
        Assert.assertEquals(order.getState(), DeliveredState.class.getName());
    }

    @Test
    public void order_should_go_to_cancelled_when_cancelled() {
        var order = new Order(1,false);
        order.submit();
        order.cancel();
        Assert.assertEquals(order.getState(), CancelledState.class.getName());
    }

    @Test
    public void order_should_go_to_provision_when_not_paid() {
        var order = new Order(1,false);
        order.submit();
        order.lastUpdated = LocalDateTime.now().minus(25, ChronoUnit.HOURS);
        order.submit();
        Assert.assertEquals(order.getState(), ProvisionalState.class.getName());
    }

    @Test
    public void order_should_go_to_cancelled_when_not_paid_as_provisioned() {
        var order = new Order(1,false);
        order.submit();
        order.lastUpdated = LocalDateTime.now().minus(24, ChronoUnit.HOURS);
        order.submit();
        order.lastUpdated = LocalDateTime.now().minus(12, ChronoUnit.HOURS);
        order.submit();
        Assert.assertEquals(order.getState(), CancelledState.class.getName());
    }
}
