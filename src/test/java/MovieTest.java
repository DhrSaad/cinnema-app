import org.junit.Assert;
import org.junit.Test;

public class MovieTest {
    @Test
    public void movie_to_string_should_return_name() {
        var movie = new Movie("Nemo");
        Assert.assertEquals("Nemo",movie.toString());
    }
}
