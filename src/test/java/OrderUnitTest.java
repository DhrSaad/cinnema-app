import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class OrderUnitTest {

    @Test
    public void order_should_return_price() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var movie_ticket = new MovieTicket(movie_screening, 1, 0, false);
        var order = new Order(1, false);
        order.addSeatReservation(movie_ticket);
        Assert.assertEquals(9.0, order.calculatePrice(), 0.0);
    }

    @Test
    public void premium_order_should_return_price_plus_three() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var movie_ticket = new MovieTicket(movie_screening, 1, 0, true);
        var order = new Order(1, false);
        order.addSeatReservation(movie_ticket);
        Assert.assertEquals(12.0, order.calculatePrice(), 0.0);
    }

    @Test
    public void premium_order_as_student_should_return_price_of_one_seat_plus_two_with_10_percent_discount() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var order = new Order(1, true);
        for (var i = 0; i < 2; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, true);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals((9.0 + 2.0) * 0.9, order.calculatePrice(), 0.0);
    }

    @Test
    public void two_tickets_as_student_should_return_price_of_one_seat() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var order = new Order(1, false);
        for (var i = 0; i < 2; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, false);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals(9.0, order.calculatePrice(), 0.0);
    }

    @Test
    public void three_tickets_should_return_price_of_two_seats() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var order = new Order(1, false);
        for (var i = 0; i < 3; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, false);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals(18.0, order.calculatePrice(), 0.0);
    }

    @Test
    public void ordering_as_a_student_should_return_price_of_one_seat_with_10_percent_discount() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var order = new Order(1, true);
        for (var i = 0; i < 2; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, false);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals(9.0 * 0.9, order.calculatePrice(), 0.0);
    }

    @Test
    public void ordering_six_tickets_on_saturday_should_return_price_of_five_seats_with_10_percent_discount() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 12, 12, 30), 9.0);
        var order = new Order(1, false);
        for (var i = 0; i < 6; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, false);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals((9.0*6.0) * 0.9, order.calculatePrice(), 0.0);
    }
    @Test
    public void ordering_six_tickets_as_student_should_return_price_of_five_seats_with_10_percent_discount() {
        var movie = new Movie("Nemo");
        var movie_screening = new MovieScreening(movie, LocalDateTime.of(2022, 2, 7, 12, 30), 9.0);
        var order = new Order(1, true);
        for (var i = 0; i < 6; i++) {
            var movie_ticket = new MovieTicket(movie_screening, 1, i, false);
            order.addSeatReservation(movie_ticket);
        }
        Assert.assertEquals((9.0*5.0) * 0.9, order.calculatePrice(), 0.0);
    }
}
