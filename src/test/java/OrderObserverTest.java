import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class OrderObserverTest {
    @Test
    public void observer_should_be_registered() {
        var order = new Order(0, false);
        var service = new SMSAdapter(order.orderState);
        var observer = new MessengerService(service);
        order.registerObserver(observer);

        Assert.assertTrue(order.observers.size() > 0);
        Assert.assertEquals(order.observers.get(0), observer);

    }

    @Test
    public void observer_should_receive_state_when_state_is_created() {
        var order = new Order(0, false);
        var service = new SMSAdapter(order.orderState);
        var observer = new MessengerService(service);
        order.registerObserver(observer);

        order.setOrderState(order.paidState);

        Assert.assertEquals(order.orderState, order.paidState);
        Assert.assertEquals(service.getOrderState(), order.paidState);
    }

    @Test
    public void observer_should_not_update_observer_when_state_is_not_special() {
        var order = new Order(0, false);
        var service = new SMSAdapter(order.orderState);
        var observer = new MessengerService(service);
        order.registerObserver(observer);
        order.setOrderState(order.paidState);

        // new paid state instance, but state is the same. don't notify the service.
        var state = new PaidState(order);
        order.setOrderState(state);

        Assert.assertEquals(order.orderState, state);
        Assert.assertNotEquals(service.getOrderState(), state);
    }
}
