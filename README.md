# Cinnema App

A Cinema application built by Sa'ad Abdullah and Ramon van Sprundel for the subject SO&A3

## Docs

### Les 2
#### Path test coverage
![](docs/UML/Path-Test-Coverage.png)

### Les 4
#### Class Diagram met states
![](docs/UML/Class-Diagram-Design-Patterns.png)

#### State Class
![](docs/UML/State-Pattern-Class.png)

#### State Transition
![](docs/UML/State-Pattern-Transition.png)

### Les 5
#### Observer/Adapter design
![](docs/UML/Observer.png)

### Les 6
#### Composite Visitor
![](docs/UML/Composite%20Visitor.drawio.png)
